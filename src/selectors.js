const params = {
	selectors: {
		login: {
			page: '#login',
			userName: '#username',
			password: '#password',
			loginBtn: '#loginbtn'
		},
		main: {
			container: '#region-main-box',
		},
		courses: {
			links: '.course_list .title a',
			content: '.course-content'
		},
	},
	delays: {
		courseContentPage: 5000,
		idle: 30 * 1000
	},
};

module.exports = params;
