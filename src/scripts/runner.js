const parser = require('./parser');
const shuffle = require('../utils/array/shuffle');

/**
 * Users JSON list
 *
 * [{"login": "login", "pass": "some_pass"}]
 */
// let usersPayload = null;

try {
	usersPayload = JSON.parse(process.env.USERS_PAYLOAD);
} catch (e) {
	console.log('Please, check "USERS_PAYLOAD" env');
	console.error(e);

	process.exit(-1);
}

// run all
(async () => {
	const users = shuffle([
		...usersPayload
	]);

	const failedUsers = [];

	for (let user of users) {
		try {
			await parser(user);
		} catch (e) {
			console.error(e);

			failedUsers.push(user);
		}
	}

	console.log(`Failed users: ${failedUsers.length}`);

	for (let u of failedUsers) {
		console.log(`Run for failed: ${u.login}`);
		await parser(u);
	}
})().catch(console.error);
