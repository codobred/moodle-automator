const path = require('path');
const fs = require('fs');
const puppeteer = require('puppeteer');


// selectors
const params = require('../selectors');

const isDev = process.env.NODE_ENV !== 'production';
const MOODLE_LOGIN_URL = 'http://moodle.kntu.kr.ua/login/index.php';

async function processCoursePage(page, href, user) {
	await page.goto(href);

	await page.waitForSelector(params.selectors.courses.content);

	const screenDir = path.join(process.cwd(), `screenshots/${user.login}`);

	await fs.promises.mkdir(screenDir, {recursive: true});

	const {searchParams} = new URL(href);
	const file = `${screenDir}/${searchParams.get('id')}.png`;

	console.log(`[screenshot] saved to: ${file}`);

	await page.screenshot({path: file});

	await page.waitFor(params.delays.courseContentPage);
}

async function login(page, user) {
	await page.goto(MOODLE_LOGIN_URL);

	const {login: loginSelectors, main: mainSelectors} = params.selectors;

	await page.waitForSelector(loginSelectors.page);

	await page.waitForSelector(loginSelectors.userName);

	await page.type(loginSelectors.userName, user.login);
	await page.type(loginSelectors.password, user.pass);
	await page.click(loginSelectors.loginBtn);

	await page.waitForSelector(mainSelectors.container);

	console.log(`${user.login} is successful authorized`);
	console.log('[navigated] to main');
}

module.exports = async function parser(user) {
	const browser = await puppeteer.launch({
		headless: !isDev,
		devtools: isDev,
	});
	const page = await browser.newPage();

	await page.setViewport({width: 1600, height: 1200});

	console.log(`Run puppeteer for: ${user.login}`);

	await login(page, user);

	const links = await page.evaluate((params) => {
		return Array.from(document.querySelectorAll(params.selectors.courses.links)).map(a => a.href).filter(Boolean);
	}, params);

	for (const link of links) {
		await processCoursePage(page, link, user);
	}

	// just stay on the site some time
	await page.waitFor(params.delays.idle);

	await browser.close();
};
